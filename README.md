# sacred-tk-dashboard

_sacred-tk-dashboard_ is a poor's man dashboard for [Sacred](https://github.com/IDSIA/sacred) using Tk. Unlike other Sacred dashboards, it reads folders created by Sacred's `FileStorageObserver`, which means you don't need to setup a MongoDB database. Perfect for lazy people !
