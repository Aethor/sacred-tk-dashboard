from pygments.lexers import get_lexer_by_name
from pygments.token import String, Name
import tkinter as tk


class HighlightedText(tk.Text):
    """"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.tag_configure("string", foreground="green")
        self.tag_configure("name", foreground="orange")

    def set_highlighted_text(self, text: str, language: str):

        self.delete("1.0", "end")

        lexer = get_lexer_by_name(language)

        tokens = lexer.get_tokens(text)

        for token_type, token in tokens:
            if token_type in String:
                self.insert("end", token, "string")
            elif token_type in Name:
                self.insert("end", token, "name")
            else:
                self.insert("end", token)
