import json
import tkinter as tk
from tkinter import StringVar, ttk
from tkinter import filedialog
from tkinter.constants import FALSE
from typing import List
from text import HighlightedText
from ttkthemes import ThemedTk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure


class MetricsCanvas:
    """"""

    def __init__(self, parent) -> None:
        self.parent = parent

        # metrics selection box
        self.metrics_var = StringVar()
        self.metrics_box = ttk.Combobox(self.parent, textvariable=self.metrics_var)
        self.metrics_box["values"] = []
        self.metrics_box.bind("<<ComboboxSelected>>", self.on_metrics_box_selected)
        self.metrics_box.grid(row=0, column=0, sticky="we")

        # plot style selection box
        self.style_var = StringVar()
        self.style_box = ttk.Combobox(self.parent, textvariable=self.style_var)
        self.style_box["values"] = []
        self.style_box.bind("<<ComboboxSelected>>", self.on_style_box_selected)
        self.style_box.grid(row=0, column=1, sticky="we")

        # matplotlib figure, canvas
        self.figure = Figure()
        self.canvas = FigureCanvasTkAgg(self.figure, master=parent)
        self.canvas.get_tk_widget().grid(row=1, column=0, columnspan=2)

    def on_metrics_box_selected(self, ev):
        self.metrics_box.selection_clear()
        self.reset_style()
        self.refresh_metrics()

    def on_style_box_selected(self, ev):
        self.refresh_metrics()

    def possible_styles(self) -> List[str]:
        metrics_name = self.metrics_var.get()
        if len(self.metrics[metrics_name]["steps"]) == 1:
            return ["text"]
        return ["plot", "scatter", "hist"]

    def reset_style(self):
        self.style_box["values"] = self.possible_styles()
        self.style_box.current(0)

    def refresh_metrics(self):
        """Refresh the canvas by reading the value of `self.selected_metrics_var`"""
        metrics_name = self.metrics_var.get()
        plot_style = self.style_var.get()

        self.figure = Figure()
        self.figure.suptitle(metrics_name)
        if plot_style == "text":
            metrics_value = self.metrics[metrics_name]["values"][0]
            self.figure.text(0.5, 0.5, str(metrics_value), ha="center")
        elif plot_style == "plot":
            self.figure.add_subplot(111).plot(
                self.metrics[metrics_name]["steps"],
                self.metrics[metrics_name]["values"],
            )
        elif plot_style == "scatter":
            self.figure.add_subplot(111).scatter(
                self.metrics[metrics_name]["steps"],
                self.metrics[metrics_name]["values"],
            )
        elif plot_style == "hist":
            self.figure.add_subplot(111).hist(
                self.metrics[metrics_name]["values"], bins="sqrt"
            )
        else:
            print(f"warning : unsupported plot type : {plot_style}")
            return

        self.canvas.get_tk_widget().destroy()
        self.canvas = FigureCanvasTkAgg(self.figure, master=self.parent)
        self.canvas.get_tk_widget().grid(row=1, column=0, columnspan=2)
        self.canvas.draw()

    def set_metrics(self, metrics: dict):
        self.metrics = metrics

        self.metrics_box["values"] = list(metrics.keys())
        if len(metrics) > 0:
            self.metrics_box.current(0)
            self.on_metrics_box_selected(None)


class Application:
    """"""

    def __init__(self, window) -> None:
        self.window = window
        self.window.title("sacred-tk-dashboard")

        # run metrics

        # menus
        ## deactivate tearoff
        ## see https://tkdocs.com/tutorial/menus.html
        self.window.option_add("*tearOff", FALSE)

        menubar = tk.Menu(self.window)

        menu_file = tk.Menu(menubar)
        menu_file.add_command(label="Open...", command=self.open_file)
        menu_file.add_command(label="Quit", command=lambda: self.window.destroy())
        menubar.add_cascade(menu=menu_file, label="File")

        self.window["menu"] = menubar

        # infos
        self.infos_frame = ttk.Frame(self.window)
        self.infos_frame.grid(row=0, column=0)

        ## config
        self.run_config = {}
        self.config_label = HighlightedText(self.infos_frame, wrap="none", width=30)
        self.config_label.grid(sticky="ew", row=0)
        ### scroll bar
        self.config_xscrollbar = tk.Scrollbar(self.infos_frame, orient=tk.HORIZONTAL)
        self.config_xscrollbar.configure(command=self.config_label.xview)
        self.config_label.configure(xscrollcommand=self.config_xscrollbar.set)
        self.config_xscrollbar.grid(sticky="ew", row=1)

        # metrics
        self.metrics = {}
        self.add_metrics_canvas_button = ttk.Button(
            self.window, text="+", command=self.add_metrics_canvas
        )
        self.remove_metrics_canvas_button = ttk.Button(
            self.window, text="-", command=self.remove_metrics_canvas
        )
        self.metrics_frames = []
        self.metrics_canvas = []
        self.add_metrics_canvas()

    def add_metrics_canvas(self):
        """Add a metrics canvas to a new metrics_frame. Also refresh
        the placement of ``self.add_metrics_canvas_button``
        accordingly.
        """
        frame = ttk.Frame(self.window)
        self.metrics_frames.append(frame)
        frame_row = (len(self.metrics_frames) - 1) // 2
        frame_column = 1 + (len(self.metrics_frames) + 1) % 2
        frame.grid(row=frame_row, column=frame_column, rowspan=2)

        metrics_canvas = MetricsCanvas(frame)
        self.metrics_canvas.append(metrics_canvas)
        metrics_canvas.set_metrics(self.metrics)

        # refresh add canvas button placement
        self.add_metrics_canvas_button.grid(
            row=frame_row, column=frame_column + 1, sticky="nsew"
        )
        self.remove_metrics_canvas_button.grid(
            row=frame_row + 1, column=frame_column + 1, sticky="nsew"
        )

    def remove_metrics_canvas(self):
        self.metrics_canvas.pop()
        self.metrics_frames.pop().destroy()
        last_frame_row = (len(self.metrics_frames) - 1) // 2
        self.add_metrics_canvas_button.grid(
            row=last_frame_row,
            column=self.add_metrics_canvas_button.grid_info()["column"] - 1,
            sticky="nsew",
        )
        self.remove_metrics_canvas_button.grid(
            row=last_frame_row + 1,
            column=self.remove_metrics_canvas_button.grid_info()["column"] - 1,
            sticky="nsew",
        )

    def open_file(self):

        directory = filedialog.askdirectory(title="Open run directory")

        if directory == "":
            return

        with open(f"{directory}/metrics.json") as f:
            self.metrics = json.load(f)

        # update metrics canvas
        for canvas in self.metrics_canvas:
            canvas.set_metrics(self.metrics)

        # update run infos
        with open(f"{directory}/config.json") as f:
            self.run_config = json.load(f)
            self.run_config = {
                k: v for k, v in self.run_config.items() if not k == "__annotations__"
            }
        self.config_label.set_highlighted_text(
            json.dumps(self.run_config, indent=2), "json"
        )


window = ThemedTk(theme="default")
app = Application(window)
window.mainloop()
